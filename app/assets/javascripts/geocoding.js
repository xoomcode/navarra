/**
 * Congo Geocoding Javascript library
 */
Navarra.namespace("geocoding");

Navarra.geocoding = function(){
  var map, infoBubbles,
    mapContainer, searchManager,
    resultSet, longitude, latitude,
    originalLat, originalLon,
    knownPlaces = [], isMapClickEnabled = false;

  var init = function(lat, lon, originalLat, oroginalLon) {
    nokia.Settings.set( "appId", "ZTVBhWvg8dw4GhrG9fcL");
    nokia.Settings.set( "authenticationToken", "jOEPEj4JkZvbAiv7GP0E2A");
    Navarra.geocoding.originalLat = originalLat;
    Navarra.geocoding.originalLon = oroginalLon;
    infoBubbles = new nokia.maps.map.component.InfoBubbles();

    mapContainer = document.getElementById("geocoding-map");
    map = new nokia.maps.map.Display(mapContainer, {
      center: [-32.934929,-68.774414],
      zoomLevel: 10,
      components: [
        new nokia.maps.map.component.Behavior(),
        new nokia.maps.map.component.ZoomBar(),
        new nokia.maps.map.component.Overview(),
        new nokia.maps.map.component.ScaleBar(),
        infoBubbles        
      ]
    });

    bindMapTypesComboChange();
    addSelectedMarker(lat, lon);
    map.addListener("click", onMapClick);
    searchManager = nokia.places.search.manager;
  },

  bindMapTypesComboChange = function() {
    map.set("baseMapType", nokia.maps.map.Display.NORMAL_COMMUNITY);

    $("#map-type").change(function() {
      map.set("baseMapType", nokia.maps.map.Display[$("#map-type option:selected").attr("value")]);
    });
  }

  onMapClick = function(evt) {
    if(!isMapClickEnabled) {
      return;
    }

    map.objects.clear();

    var coord = map.pixelToGeo(evt.displayX, evt.displayY);
    addSelectedMarker(coord.latitude, coord.longitude);
  },

  enableMapClick = function(enabled) {
    isMapClickEnabled = enabled;
  },

  addOriginalMarker = function() {
    if(!Navarra.geocoding.originalLat || !Navarra.geocoding.originalLon) {
      return;
    }

    var options = {
      draggable : false,
      text: "I",
      brush : {
        color : "#FF9900"
      }
    }

    return addMarker(Navarra.geocoding.originalLat, Navarra.geocoding.originalLon, options);
  },

  addSelectedMarker = function(lat, lon) {
    if(!lat || !lon) {
      return;
    }

    Navarra.geocoding.latitude = lat;
    Navarra.geocoding.longitude = lon;

    var options = {
      draggable : false,
      text: "S",
      brush : {
        color : "#007f00"
      }
    }

    addOriginalMarker();
    addMarker(lat, lon, options);
    map.zoomTo(map.getBoundingBox(), false);
  },

  addMarker = function(lat, lon, options) {
    var coor = new nokia.maps.geo.Coordinate(lat, lon)
    var marker = new nokia.maps.map.StandardMarker(coor, options);
    marker.addListener("click", onMarkerClick);
    map.objects.addAll([marker]);
    return marker;
  },

  doReverseGeocode = function(opt) {
    var reverseGeoCodeTerm = new nokia.maps.geo.Coordinate(parseFloat(opt["latitude"]), parseFloat(opt["longitude"]));
    map.objects.clear();
    $("#geocoding-map").trigger("geocodingStarted");
    $("body").css("cursor", "progress");

    searchManager.reverseGeoCode({
      latitude: reverseGeoCodeTerm.latitude,
      longitude: reverseGeoCodeTerm.longitude,
      onComplete: processResults
    });
  },

  doGeocode = function(opt){
    var options = {searchTerm: opt.searchTerm, onComplete: processResults}
    if(opt["location"] != null) {
      options["location"] = opt["location"];
    }

    if(opt["county"] != null) {
      options["county"] = opt["county"];
    }

    if(opt["country"] != null) {
      options["country"] = opt["country"];
    }

    if(opt.bbox && opt.bbox.top && opt.bbox.bottom &&
      opt.bbox.top.latitude && opt.bbox.top.longitude &&
      opt.bbox.bottom.latitude && opt.bbox.bottom.longitude) {

      var boundingBox = {
        topLeft: {
          longitude: opt.bbox.top.longitude,
          latitude: opt.bbox.top.latitude
        },
        bottomRight: {
          longitude: opt.bbox.bottom.longitude,
          latitude: opt.bbox.bottom.latitude
        }
      };

      options.boundingBox = boundingBox;
    }

    map.objects.clear();
    $("#geocoding-map").trigger("geocodingStarted");
    $("body").css("cursor", "progress");
    knownPlaces = [];

    if(opt.knownLocationsUrl) {
      options["knownLocationsUrl"] = opt.knownLocationsUrl;
      findKnownPlaces(options);

    } else {
      findPlaces(options);
    }
  },

  findPlaces = function(options) {
    var address = options.searchTerm;
    
    if(options["location"] != null) {

      if(options["county"] != null && options["location"] == "Capital") {
        address += ", " + options["county"];

      } else {
        address += ", " + options["location"];  
      }      
    }

    if(options["county"] != null) {
      address += ", " + options["county"];
    }

    if(options["country"] != null) {
      address += ", " + options["country"];
    }

    console.log(address);

    options.searchTerm = address
    searchManager.findPlaces(options);
  },

  findKnownPlaces = function(options) {
    $.ajax({
      url: options.knownLocationsUrl,
      type: "GET",
      data: {"search_term": options.searchTerm},
      success: function(result) {
        knownPlaces = result;
        findPlaces(options);
      }
    });
  },

  processResults = function(data, requestStatus, requestId) {
    $("body").css("cursor", "auto");
    var i, len, locations, marker;
    var knownMarkerOptions = {draggable : false, text: "E", brush : {color : "#FF9900"}}

    if (requestStatus == "OK") {
      locations = data.results ? data.results.items : [data.location];

      if (locations.length > 0 || knownPlaces.length > 0) {
        if (resultSet) map.objects.remove(resultSet);   
        resultSet = new nokia.maps.map.Container();
        resultSet.addListener("click", onMarkerClick);

        if(addOriginalMarker()) {
          var originalMarker = new nokia.maps.map.StandardMarker(
            {"latitude": parseFloat(Navarra.geocoding.originalLat),
            "longitude": parseFloat(Navarra.geocoding.originalLon)},
            {draggable : false, text: "I", brush : {color : "#FF9900"}});
          resultSet.objects.add(originalMarker);
        }

        for (i = 0, len = locations.length; i < len; i++) {          
          marker = new nokia.maps.map.StandardMarker(locations[i].position, {text: i+1});
          marker.$label = locations[i].title; 
          marker.$address = locations[i].vicinity;
          resultSet.objects.add(marker);
          marker.addListener("mouseover", onMarkerMouseOver);
        }

        $.each(knownPlaces, function(i, point){
          marker = new nokia.maps.map.StandardMarker({"latitude": parseFloat(point.lat), "longitude": parseFloat(point.lon)}, knownMarkerOptions);
          resultSet.objects.add(marker);
        });

        $("#geocoding-map").trigger("markersShown");
        
        map.objects.add(resultSet);
        map.zoomTo(resultSet.getBoundingBox(), false);

      } else {
        alert("Su búsqueda no produjo resultados!");
      }
      
    } else {
      alert("La búsqueda falló");
    }
  },

  onMarkerMouseOver = function(evt) {
    var marker = evt.target,
    label = marker.$label,
    address = marker.$address;
  
    if (marker instanceof nokia.maps.map.Marker) 
      infoBubbles.addBubble(label, marker.coordinate, false);
  },

  onMarkerClick = function(evt) {
    map.objects.clear();
    map.objects.remove(evt.target);
    addSelectedMarker(evt.target.coordinate.latitude, evt.target.coordinate.longitude);
  };

  return {
    init: init,
    doGeocode: doGeocode,
    doReverseGeocode: doReverseGeocode,
    addSelectedMarker: addSelectedMarker,
    longitude: longitude,
    latitude: latitude,
    enableMapClick: enableMapClick
  }
}();