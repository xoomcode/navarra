Navarra.namespace("pois.action_new");
Navarra.namespace("pois.action_edit");
Navarra.namespace("pois.action_update");
Navarra.namespace("pois.action_create");
Navarra.namespace("pois.action_index");

Navarra.pois.config = {
  "lon": null,
  "lat": null,
  "originalLat": null,
  "originalLon": null
};

Navarra.pois.action_index = function() {
  var init = function() {
    Navarra.poi_search_panel.init();
  }

  return {
    init: init
  }
}();

Navarra.pois.action_edit = function() {
  var initCitiesChosen = function() {
    $("#poi_city_id").ajaxChosen({
      type: 'GET',
      url: '/locations/cities',
      dataType: 'json'

    }, function (data) {
      var results = [];

      $.each(data, function (i, val) {
        results.push({value: val.value, text: val.text});
      });

      return results;
    });
  },

  loadPoiSubTypesCombo = function(result) {
    Navarra.common.form.loadComboOptions("#poi_poi_sub_type_id", result, "name");
  },

  loadChainsCombo = function(result) {
    Navarra.common.form.loadComboOptions("#poi_chain_id", result, "name");
  },

  loadFoodTypesCombo = function(result) {
    Navarra.common.form.loadComboOptions("#poi_food_type_id", result, "name");
  },

  loadLatLonFields = function() {
    $(".btn-primary").click(function(e) {
      $("#poi_latitude").attr("value", Navarra.geocoding.latitude);
      $("#poi_longitude").attr("value", Navarra.geocoding.longitude);
    });
  },

  bindAddMarkerButtonClick = function() {
    $("#add-marker-btn").click(function(e) {
      e.preventDefault();
      $(this).toggleClass("btn-success");
      Navarra.geocoding.enableMapClick($(this).hasClass("btn-success"));
    });
  },

  bindSuggestButtonClick = function() {
    $("#suggest-locations-btn").click(function(e) {
      e.preventDefault();

      if($("#poi_street_name").val() == '') {
        alert("Debe ingresar una Calle para poder sugerir ubicaciones.");
        return;
      }

      if($("#poi_house_number").val() == '') {
        alert("Debe ingresar Número para poder sugerir ubicaciones.");
        return;
      }

      var searchTerm = $("#poi_street_name").val() + " " + $("#poi_house_number").val();
      var options = {"searchTerm": searchTerm};

      if($("#poi_city_id").val() == '') {
        alert("Debe seleccionar una ciudad para poder sugerir ubicaciones.");
        return;
      }

      var location = $("#poi_city_id option:selected").text();
      var locationArr = location.split(", ");

      if(locationArr[1] != undefined) {
        options["location"] = locationArr[1];
      }

      if(locationArr[2] != undefined) {
        options["county"] = locationArr[2];
      }

      if(locationArr[3] != undefined) {
        options["country"] = locationArr[3];
      }

      Navarra.geocoding.doGeocode(options);
    });
  },

  bindPoiTypesComboChange = function() {
    $("#poi_poi_type_id").change(function() {
      if($(this).val() == '') {
        Navarra.common.form.loadComboOptions("#poi_poi_sub_type_id", [], "name");
        Navarra.common.form.loadComboOptions("#poi_chain_id", [], "name");
        Navarra.common.form.loadComboOptions("#poi_food_type_id", [], "name");
        return;
      }

      var poiTypeUrl = "/poi_types/" + $(this).val();
      var subTpesUrl = poiTypeUrl + "/sub_types";
      var chainsUrl = poiTypeUrl + "/chains";
      var foodUrl = poiTypeUrl + "/food_types";
      Navarra.common.request.getAjax(subTpesUrl, loadPoiSubTypesCombo);
      //Navarra.common.request.getAjax(chainsUrl, loadChainsCombo); Descomentar si se requiere filtrar la cadena por tipo de poi
      Navarra.common.request.getAjax(foodUrl, loadFoodTypesCombo);
    })
  },

  initControlDatePicker = function() {
    $('#poi_control_date').datepicker({dateFormat: 'dd-mm-yy'});
  };

  var init = function() {
    initCitiesChosen();
    bindPoiTypesComboChange();
    Navarra.geocoding.init(
      Navarra.pois.config.lat,
      Navarra.pois.config.lon,
      Navarra.pois.config.originalLat,
      Navarra.pois.config.originalLon);
    loadLatLonFields();
    bindAddMarkerButtonClick();
    bindSuggestButtonClick();
    initControlDatePicker();
  };

  return {
    init: init
  }
}();

Navarra.pois.action_new = function() {
  var bindInputFocusOut = function() {
    $("input").focusout(function(e) {
      f = $(this).closest("form").serialize();
      $.getScript('/pois/possible_duplicates.js?' + f);
    });    
  };

  var init = function() {
    Navarra.pois.action_edit.init();
    bindInputFocusOut();
  }
  
  return {
    init: init
  }
}();

Navarra.pois.action_update = function() {
  var init = function() {
    Navarra.pois.action_edit.init();
  }

  return {
    init: init
  }
}();

Navarra.pois.action_create = function() {
  var init = function() {
    Navarra.pois.action_new.init();
  }

  return {
    init: init
  }
}();