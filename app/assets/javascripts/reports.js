Navarra.namespace("reports.action_pois");
Navarra.namespace("reports.action_users");

Navarra.reports.action_pois = function() {
  var init = function() {
    Navarra.poi_search_panel.init();
  }

  return {
    init: init
  }
}();

Navarra.reports.action_users = function() {
  var init = function() {
    Navarra.poi_search_panel.init();
  }

  return {
    init: init
  }
}();