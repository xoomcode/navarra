class ChainsController < ApplicationController
  before_filter :new_chain, :only => [:create]
  load_and_authorize_resource
  before_action :set_chain, only: [:show, :edit, :update, :destroy]

  # GET /chains
  # GET /chains.json
  def index
    @chains = Chain.sorted_by_name.paginate(:page => params[:page])
  end

  # GET /chains/1
  # GET /chains/1.json
  def show
  end

  # GET /chains/new
  def new
  end

  # GET /chains/1/edit
  def edit
  end

  # POST /chains
  # POST /chains.json
  def create
    respond_to do |format|
      if @chain.save
        format.html { redirect_to @chain, flashman.create_success }
        format.json { render action: 'show', status: :created, location: @chain }
      else
        flashman.create_fail
        format.html { render action: 'new' }
        format.json { render json: @chain.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /chains/1
  # PATCH/PUT /chains/1.json
  def update
    respond_to do |format|
      if @chain.update(chain_params)
        format.html { redirect_to @chain, flashman.update_success }
        format.json { head :no_content }
      else
        flashman.update_fail
        format.html { render action: 'edit' }
        format.json { render json: @chain.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chains/1
  # DELETE /chains/1.json
  def destroy
    if @chain.destroy
      flashman.destroy_success
    else
      flashman.destroy_fail @chain
    end

    respond_to do |format|
      format.html { redirect_to chains_url }
      format.json { head :no_content }
    end
  end

  private
    def set_chain
      @chain = Chain.find(params[:id])
    end

    def new_chain
      @chain = Chain.new(chain_params)
    end

    def chain_params
      params.require(:chain).permit(:name, :identifier, :poi_type_id)
    end
end
