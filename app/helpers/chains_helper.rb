module ChainsHelper
  def chains_for_select
    Chain.sorted_by_name.map { |chain| [chain.name, chain.id] }
  end
end
