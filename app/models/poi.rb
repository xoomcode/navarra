class Poi < ActiveRecord::Base
  extend Navarra::Poi::Download
  include Navarra::Poi::Attributes
  extend Navarra::Poi::Finders  

  has_paper_trail
  set_rgeo_factory_for_column(:the_geom, RGeo::Geographic.spherical_factory(:srid => 4326))

  belongs_to :poi_type
  belongs_to :poi_load
  belongs_to :chain
  belongs_to :poi_status
  belongs_to :poi_sub_type
  belongs_to :food_type
  belongs_to :poi_source
  belongs_to :street_type
  belongs_to :user
  belongs_to :city
  belongs_to :p_action
  has_one :department, :through => :city
  has_one :province, :through => :department
  has_one :country, :through => :province

  before_validation :build_identifier
  before_validation :build_geom
  before_save :build_geom
  before_save :load_identifier_hash
  after_save :add_dictionary_terms

  validates :poi_type_id, :presence => true
  validates :poi_source_id, :presence => true, :if => :was_validated?
  validate :control_date_present?
  validates :name, :presence => true, :if => :was_validated?
  validates :user_id, :presence => true, :if => :was_validated?
  validates :city_id, :presence => true  
  validates :poi_status_id, :presence => true
  validates :identifier, :presence => true, :uniqueness => true, :unless => :poi_without_source?
  #validates :old_identifier, :presence => true, :if => :is_navteq_poi?

  validates :email, :second_email,
    :email_format => {:message => I18n.t("activerecord.errors.messages.invalid_email"),
    :allow_nil => true, :allow_blank => true}, :if => :was_validated?

  validates :phone, :second_phone, :cel_number,
    :format => {:with => %r{\A(([0-9]{1,4})([\-]{1})([\d]{1,10}))?\z}i,
    :message => I18n.t("activerecord.errors.messages.invalid_phone")},
    :allow_nil => true, :allow_blank => true, :if => :was_validated?

  validates :website,
    :format => {:with => %r{\Ahttps?:\/\/([^\s:@]+:[^\s:@]*@)?[A-Za-z\d\-]+(\.[A-Za-z\d\-]+)+\.?(:\d{1,5})?([\/?]\S*)?\z}i,
    :message => I18n.t("activerecord.errors.messages.invalid_url")},
    :allow_nil => true, :allow_blank => true, :if => :was_validated?

  validate :geometry_setted?
  validate :duplicated_identifier_exist?

  delegate :complete_name, :to => :city, :prefix => true, :allow_nil => true
  delegate :name, :to => :country, :prefix => true, :allow_nil => true
  delegate :name, :to => :province, :prefix => true, :allow_nil => true
  delegate :name, :to => :department, :prefix => true, :allow_nil => true
  delegate :name, :to => :city, :prefix => true, :allow_nil => true
  delegate :zip, :to => :city, :prefix => true, :allow_nil => true
  delegate :name, :to => :chain, :prefix => true, :allow_nil => true
  delegate :identifier, :to => :chain, :prefix => true, :allow_nil => true
  delegate :name, :to => :poi_source, :prefix => true, :allow_nil => true
  delegate :name, :to => :poi_source, :prefix => true, :allow_nil => true
  delegate :name, :to => :food_type, :prefix => true, :allow_nil => true
  delegate :name, :to => :street_type, :prefix => true, :allow_nil => true
  delegate :name, :to => :poi_type, :prefix => true, :allow_nil => true
  delegate :name, :to => :poi_sub_type, :prefix => true, :allow_nil => true
  delegate :name, :to => :user, :prefix => true, :allow_nil => true
  delegate :some_identifier, :to => :user, :allow_nil => true
  delegate :human_name, :to => :poi_status, :prefix => true, :allow_nil => true
  delegate :name, :to => :poi_status, :prefix => true, :allow_nil => true

  attr_accessor :latitude, :longitude

  def self.deliver pois
    count = 0
    pois.each do |poi|
      if (poi.poi_status_id == PoiStatus.validated.id) and poi.active
        pp = Poi.find_by_id(poi.id) #workaround in order to avoid ActiveRecord::ReadOnlyRecord error
        pp.poi_status_id = PoiStatus.delivered.id
        count += 1 if pp.save
      end
    end
    count
  end

  def original_object
    begin
      return self.versions.second.reify
    rescue
      return Poi.find_by_id(self.id)
    end
  end

  def was_validated?
    (self.poi_status_id == PoiStatus.validated.id or
     self.poi_status_id == PoiStatus.delivered.id)
  end

  private

    def is_navteq_poi?
      (self.poi_source_id == PoiSource.navteq.id)
    end

    def poi_without_source?
      self.poi_source.nil?
    end

    def duplicated_identifier_exist?
      return true if self.duplicated_identifier.nil?
      duplicated = Poi.find_by_old_identifier self.duplicated_identifier

      if !duplicated
        self.errors.add(:duplicated_identifier, :nonexistent)
        return false
      end

      if duplicated.id == self.id
        self.errors.add(:duplicated_identifier, :same_original_and_duplicated)
        return false
      end
    end

    def control_date_present?
      return true unless was_validated?
      if self.control_date.nil?
        self.errors.add(:base, :undefined_control_date)
        return false
      end
    end

    def geometry_setted?
      if self.the_geom.nil?
        self.errors.add(:base, :empty_geometry)
        return false
      end
    end

    def build_identifier
      if self.identifier.nil? and
        self.poi_source_id == PoiSource.gisworking.id
        last_gis_poi_identifier = Poi.all_gisworking.order(:identifier).try(:last).try(:identifier)
        gisworking_initial_identifier = AppConfiguration.first.gisworking_initial_identifier

        if last_gis_poi_identifier and
          (last_gis_poi_identifier + 1) > gisworking_initial_identifier
          self.identifier = (last_gis_poi_identifier + 1)

        else
          self.identifier = gisworking_initial_identifier
        end
      end
    end

    def build_geom
      if self.latitude and self.longitude and
        !self.latitude.to_s.empty? and !self.longitude.to_s.empty?
        self.the_geom = "POINT(#{self.longitude} #{self.latitude})"
      end
    end

    def load_identifier_hash
      self.identifier_hash = Digest::MD5.hexdigest(
        (self.name.to_s.delete(" ").downcase + 
        self.poi_type_id.to_s.delete(" ").downcase + 
        self.street_name.to_s.delete(" ").downcase + 
        self.house_number.to_s.delete(" ").downcase + 
        self.city_id.to_s.delete(" ").downcase))
    end

    def add_dictionary_terms
      self.name.to_s.split.each do |term|
        c = Poi.where("pois.name ~* '(\ |^)#{Regexp.escape(term.gsub(/'/, "\'\'"))}(\ |$)'").count
        d = Poi.where("pois.name ilike '%#{Regexp.escape(self.name.gsub(/'/, "\'\'"))}%'").count
        if ((c - d) > 10 )
          Term.find_or_create_by_name(term)
        end
      end
    end    
end
