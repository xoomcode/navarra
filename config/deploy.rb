require "rvm/capistrano"
require "bundler/capistrano"

role :web
role :app
role :db

set :application, "navarra"
set :repository,  "git@bitbucket.org:xoomcode/navarra.git"
# set :repository,  "git@git.xoomcode.com:navarra"
set :scm, :git
set :scm_username, "git"
set :user, "webapps"
set :deploy_to, "/opt/navarra"
set :rails_env,      "production"
set :passenger_port, 1083
set :passenger_cmd,  "passenger"
set :bundle_cmd, "bundle"
set :rvm_type, :system
set :rvm_ruby_string, '1.9.3@navarra'

after "deploy:update_code", "deploy:back_label"

desc 'configure task for deployment in poi.xoomcode.com, run `cap poi deploy`'
task :poi do
  server "lavalle.gisworking.com", :app, :web, :db, :primary => true
end

desc 'configure task for deployment in navarra.xoomcode.com (this is the default), run `cap navarra deploy`'
task :navarra do
  server "pellegrini.gisworking.com", :app, :web, :db, :primary => true
end

namespace :deploy do
  task :start  do
    run "sudo /etc/init.d/passenger start navarra"
  end

  task :stop do
    run "sudo /etc/init.d/passenger stop navarra"
  end

  task :restart do
    run "sudo /etc/init.d/passenger restart navarra"
  end

  task :back_label do
    run "cd #{current_release} && git rev-parse HEAD > public/build.txt"
    run "cd #{current_release} && date >> public/build.txt"
  end
end
