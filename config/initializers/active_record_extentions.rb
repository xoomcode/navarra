class ActiveRecord::Base
  def self.model_attributes
  	self.new.attributes.keys
  end

  def self.model_downcase
  	self.model_name.to_s.underscore
  end
end