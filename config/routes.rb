Navarra::Application.routes.draw do
  get "reports/pois"
  get "reports/users"

  get 'locations/cities' => 'locations#cities'
  get 'country/:id/provinces' => 'locations#provinces'
  get 'province/:id/departments' => 'locations#departments'
  get 'department/:id/cities' => 'locations#department_cities'

  get 'poi_types/:id/sub_types' => 'poi_types#sub_types'
  get 'poi_types/:id/chains' => 'poi_types#chains'
  get 'poi_types/:id/food_types' => 'poi_types#food_types'

  get 'configuration' => 'configuration#index'
  put 'configuration/:id' => 'configuration#update', :as => :update_configuration

  resources :pois do
    collection do
      get :download
      get :search
      get :deliver
      get :duplicated
      get :possible_duplicates
    end
  end

  resources :poi_loads, :except => [:edit, :update] do
    collection do
      get :download_xls_example
    end
    member do
      get :download_source_file
      get :download_errors_file
    end
  end

  resources :poi_sub_types
  resources :poi_sources
  resources :street_types
  resources :food_types
  resources :poi_types
  resources :chains
  devise_for :users
  resources :users
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'pois#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
