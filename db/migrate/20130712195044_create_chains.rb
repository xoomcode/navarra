class CreateChains < ActiveRecord::Migration
  def change
    create_table :chains do |t|
      t.string :name
      t.string :identifier
      t.integer :poi_type_id

      t.timestamps
    end
  end
end
