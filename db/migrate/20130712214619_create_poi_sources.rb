class CreatePoiSources < ActiveRecord::Migration
  def change
    create_table :poi_sources do |t|
      t.string :name

      t.timestamps
    end
  end
end
