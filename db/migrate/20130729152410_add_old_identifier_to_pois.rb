class AddOldIdentifierToPois < ActiveRecord::Migration
  def change
    add_column :pois, :old_identifier, :integer
  end
end
