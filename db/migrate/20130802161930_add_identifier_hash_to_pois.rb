class AddIdentifierHashToPois < ActiveRecord::Migration
  def change
    add_column :pois, :identifier_hash, :string
  end
end
