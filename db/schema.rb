# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140321191518) do

  create_table "actions", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "app_configurations", force: true do |t|
    t.integer  "gisworking_initial_identifier"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "chains", force: true do |t|
    t.string   "name"
    t.string   "identifier"
    t.integer  "poi_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cities", force: true do |t|
    t.string   "name"
    t.integer  "department_id"
    t.string   "zip"
    t.integer  "proiority"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "departments", force: true do |t|
    t.string   "name"
    t.integer  "province_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "food_types", force: true do |t|
    t.string   "name"
    t.integer  "poi_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "p_actions", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "poi_loads", force: true do |t|
    t.string   "name"
    t.datetime "load_date"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "success_count"
    t.integer  "fail_count"
    t.integer  "already_loaded_count"
    t.string   "directory_name"
  end

  create_table "poi_sources", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "poi_statuses", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "poi_sub_types", force: true do |t|
    t.string   "name"
    t.integer  "poi_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "poi_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pois", force: true do |t|
    t.string   "name"
    t.string   "short_name"
    t.string   "website"
    t.string   "email"
    t.string   "second_email"
    t.text     "note"
    t.string   "cel_number"
    t.string   "phone"
    t.string   "second_phone"
    t.string   "fax"
    t.string   "house_number"
    t.text     "contact"
    t.integer  "priority"
    t.text     "location"
    t.integer  "city_id"
    t.integer  "chain_id"
    t.integer  "food_type_id"
    t.integer  "poi_source_id"
    t.integer  "poi_type_id"
    t.integer  "poi_sub_type_id"
    t.string   "street_name"
    t.integer  "street_type_id"
    t.integer  "user_id"
    t.integer  "poi_status_id"
    t.boolean  "active",                                                      default: true
    t.boolean  "deleted",                                                     default: false
    t.integer  "duplicated_identifier"
    t.integer  "identifier"
    t.date     "control_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.spatial  "the_geom",              limit: {:srid=>4326, :type=>"point"}
    t.integer  "poi_load_id"
    t.integer  "old_identifier"
    t.string   "identifier_hash"
    t.integer  "p_action_id"
  end

  add_index "pois", ["the_geom"], :name => "index_pois_on_the_geom", :spatial => true

  create_table "pois_actualizar", id: false, force: true do |t|
    t.integer  "id"
    t.string   "name"
    t.string   "short_name"
    t.string   "website"
    t.string   "email"
    t.string   "second_email"
    t.text     "note"
    t.string   "cel_number"
    t.string   "phone"
    t.string   "second_phone"
    t.string   "fax"
    t.string   "house_number"
    t.text     "contact"
    t.integer  "priority"
    t.text     "location"
    t.integer  "city_id"
    t.integer  "chain_id"
    t.integer  "food_type_id"
    t.integer  "poi_source_id"
    t.integer  "poi_type_id"
    t.integer  "poi_sub_type_id"
    t.string   "street_name"
    t.integer  "street_type_id"
    t.integer  "user_id"
    t.integer  "poi_status_id"
    t.boolean  "active"
    t.boolean  "deleted"
    t.integer  "duplicated_identifier"
    t.integer  "identifier"
    t.date     "control_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.spatial  "the_geom",              limit: {:no_constraints=>true}
    t.integer  "poi_load_id"
    t.integer  "old_identifier"
    t.string   "identifier_hash"
  end

  create_table "pois_duplicados", id: false, force: true do |t|
    t.integer  "id"
    t.string   "name"
    t.string   "short_name"
    t.string   "website"
    t.string   "email"
    t.string   "second_email"
    t.text     "note"
    t.string   "cel_number"
    t.string   "phone"
    t.string   "second_phone"
    t.string   "fax"
    t.string   "house_number"
    t.text     "contact"
    t.integer  "priority"
    t.text     "location"
    t.integer  "city_id"
    t.integer  "chain_id"
    t.integer  "food_type_id"
    t.integer  "poi_source_id"
    t.integer  "poi_type_id"
    t.integer  "poi_sub_type_id"
    t.string   "street_name"
    t.integer  "street_type_id"
    t.integer  "user_id"
    t.integer  "poi_status_id"
    t.boolean  "active"
    t.boolean  "deleted"
    t.integer  "duplicated_identifier"
    t.integer  "identifier"
    t.date     "control_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.spatial  "the_geom",              limit: {:no_constraints=>true}
    t.integer  "poi_load_id"
    t.integer  "old_identifier"
    t.string   "identifier_hash"
  end

  create_table "pois_eliminar", id: false, force: true do |t|
    t.integer  "id"
    t.string   "name"
    t.string   "short_name"
    t.string   "website"
    t.string   "email"
    t.string   "second_email"
    t.text     "note"
    t.string   "cel_number"
    t.string   "phone"
    t.string   "second_phone"
    t.string   "fax"
    t.string   "house_number"
    t.text     "contact"
    t.integer  "priority"
    t.text     "location"
    t.integer  "city_id"
    t.integer  "chain_id"
    t.integer  "food_type_id"
    t.integer  "poi_source_id"
    t.integer  "poi_type_id"
    t.integer  "poi_sub_type_id"
    t.string   "street_name"
    t.integer  "street_type_id"
    t.integer  "user_id"
    t.integer  "poi_status_id"
    t.boolean  "active"
    t.boolean  "deleted"
    t.integer  "duplicated_identifier"
    t.integer  "identifier"
    t.date     "control_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.spatial  "the_geom",              limit: {:no_constraints=>true}
    t.integer  "poi_load_id"
    t.integer  "old_identifier"
    t.string   "identifier_hash"
  end

  create_table "pois_sin_duplicados", id: false, force: true do |t|
    t.integer  "id"
    t.string   "name"
    t.string   "short_name"
    t.string   "website"
    t.string   "email"
    t.string   "second_email"
    t.text     "note"
    t.string   "cel_number"
    t.string   "phone"
    t.string   "second_phone"
    t.string   "fax"
    t.string   "house_number"
    t.text     "contact"
    t.integer  "priority"
    t.text     "location"
    t.integer  "city_id"
    t.integer  "chain_id"
    t.integer  "food_type_id"
    t.integer  "poi_source_id"
    t.integer  "poi_type_id"
    t.integer  "poi_sub_type_id"
    t.string   "street_name"
    t.integer  "street_type_id"
    t.integer  "user_id"
    t.integer  "poi_status_id"
    t.boolean  "active"
    t.boolean  "deleted"
    t.integer  "duplicated_identifier"
    t.integer  "identifier"
    t.date     "control_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.spatial  "the_geom",              limit: {:no_constraints=>true}
    t.integer  "poi_load_id"
    t.integer  "old_identifier"
    t.string   "identifier_hash"
  end

  create_table "provinces", force: true do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "street_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "terms", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "role"
    t.string   "name"
    t.string   "email",               default: "", null: false
    t.string   "encrypted_password",  default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], :name => "index_versions_on_item_type_and_item_id"

end
