# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def create_user email, name, pass, role
	u = User.find_by_email email
	User.delete(u.id) unless u.nil?

	options = {
	  :name => name,
	  :email => email,
	  :password => pass,      
	  :role => role
	}

	user = User.create(options)
end

AppConfiguration.delete_all
AppConfiguration.create(:gisworking_initial_identifier => 500000)
  
create_user "super@admin.com", "Admin", "superadmin", "Admin"

PoiSource.find_or_create_by_name("Navteq")
PoiSource.find_or_create_by_name("Gisworking")

PoiStatus.find_or_create_by_name("not_validated")
PoiStatus.find_or_create_by_name("validated")
PoiStatus.find_or_create_by_name("delivered")

# Creamos los actions

PAction.find_or_create_by(name: "New")
PAction.find_or_create_by(name: "Update")
PAction.find_or_create_by(name: "Keep")
PAction.find_or_create_by(name: "Delete")
PAction.find_or_create_by(name: "No Info")
