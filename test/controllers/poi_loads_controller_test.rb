require 'test_helper'

class PoiLoadsControllerTest < ActionController::TestCase
  setup do
    @poi_load = poi_loads(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:poi_loads)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create poi_load" do
    assert_difference('PoiLoad.count') do
      post :create, poi_load: { load_date: @poi_load.load_date, name: @poi_load.name, status: @poi_load.status }
    end

    assert_redirected_to poi_load_path(assigns(:poi_load))
  end

  test "should show poi_load" do
    get :show, id: @poi_load
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @poi_load
    assert_response :success
  end

  test "should update poi_load" do
    patch :update, id: @poi_load, poi_load: { load_date: @poi_load.load_date, name: @poi_load.name, status: @poi_load.status }
    assert_redirected_to poi_load_path(assigns(:poi_load))
  end

  test "should destroy poi_load" do
    assert_difference('PoiLoad.count', -1) do
      delete :destroy, id: @poi_load
    end

    assert_redirected_to poi_loads_path
  end
end
