require 'test_helper'

class PoiSourcesControllerTest < ActionController::TestCase
  setup do
    @poi_source = poi_sources(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:poi_sources)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create poi_source" do
    assert_difference('PoiSource.count') do
      post :create, poi_source: { name: @poi_source.name }
    end

    assert_redirected_to poi_source_path(assigns(:poi_source))
  end

  test "should show poi_source" do
    get :show, id: @poi_source
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @poi_source
    assert_response :success
  end

  test "should update poi_source" do
    patch :update, id: @poi_source, poi_source: { name: @poi_source.name }
    assert_redirected_to poi_source_path(assigns(:poi_source))
  end

  test "should destroy poi_source" do
    assert_difference('PoiSource.count', -1) do
      delete :destroy, id: @poi_source
    end

    assert_redirected_to poi_sources_path
  end
end
