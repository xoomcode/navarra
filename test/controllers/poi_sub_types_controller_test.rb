require 'test_helper'

class PoiSubTypesControllerTest < ActionController::TestCase
  setup do
    @poi_sub_type = poi_sub_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:poi_sub_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create poi_sub_type" do
    assert_difference('PoiSubType.count') do
      post :create, poi_sub_type: { name: @poi_sub_type.name, poi_type: @poi_sub_type.poi_type }
    end

    assert_redirected_to poi_sub_type_path(assigns(:poi_sub_type))
  end

  test "should show poi_sub_type" do
    get :show, id: @poi_sub_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @poi_sub_type
    assert_response :success
  end

  test "should update poi_sub_type" do
    patch :update, id: @poi_sub_type, poi_sub_type: { name: @poi_sub_type.name, poi_type: @poi_sub_type.poi_type }
    assert_redirected_to poi_sub_type_path(assigns(:poi_sub_type))
  end

  test "should destroy poi_sub_type" do
    assert_difference('PoiSubType.count', -1) do
      delete :destroy, id: @poi_sub_type
    end

    assert_redirected_to poi_sub_types_path
  end
end
