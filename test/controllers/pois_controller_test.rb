require 'test_helper'

class PoisControllerTest < ActionController::TestCase
  setup do
    @poi = pois(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pois)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create poi" do
    assert_difference('Poi.count') do
      post :create, poi: { active: @poi.active, cel_number: @poi.cel_number, chain_id: @poi.chain_id, city_id: @poi.city_id, contact: @poi.contact, deleted: @poi.deleted, identifier: @poi.identifier, duplicated_identifier: @poi.duplicated_identifier, email: @poi.email, fax: @poi.fax, food_type_id: @poi.food_type_id, house_number: @poi.house_number, location: @poi.location, name: @poi.name, note: @poi.note, phone: @poi.phone, poi_source_id: @poi.poi_source_id, poi_status_id: @poi.poi_status_id, poi_sub_type_id: @poi.poi_sub_type_id, priority: @poi.priority, second_email: @poi.second_email, second_phone: @poi.second_phone, short_name: @poi.short_name, street_name: @poi.street_name, street_type_id: @poi.street_type_id, the_geom: @poi.the_geom, user_id: @poi.user_id, website: @poi.website }
    end

    assert_redirected_to poi_path(assigns(:poi))
  end

  test "should show poi" do
    get :show, id: @poi
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @poi
    assert_response :success
  end

  test "should update poi" do
    patch :update, id: @poi, poi: { active: @poi.active, cel_number: @poi.cel_number, chain_id: @poi.chain_id, city_id: @poi.city_id, contact: @poi.contact, deleted: @poi.deleted, identifier: @poi.identifier, duplicated_identifier: @poi.duplicated_identifier, email: @poi.email, fax: @poi.fax, food_type_id: @poi.food_type_id, house_number: @poi.house_number, location: @poi.location, name: @poi.name, note: @poi.note, phone: @poi.phone, poi_source_id: @poi.poi_source_id, poi_status_id: @poi.poi_status_id, poi_sub_type_id: @poi.poi_sub_type_id, priority: @poi.priority, second_email: @poi.second_email, second_phone: @poi.second_phone, short_name: @poi.short_name, street_name: @poi.street_name, street_type_id: @poi.street_type_id, the_geom: @poi.the_geom, user_id: @poi.user_id, website: @poi.website }
    assert_redirected_to poi_path(assigns(:poi))
  end

  test "should destroy poi" do
    assert_difference('Poi.count', -1) do
      delete :destroy, id: @poi
    end

    assert_redirected_to pois_path
  end
end
